// 인스턴스 : heap 영역에 올라옴. 
//	       new 함수를 사용하여 클래스를 객체화하는 것 : 인스턴스화, 객체화
// 객체 : 메모리에 올라와 있는 인스턴스
// 클래스 : 속성(멤버변수) + 기능(메소드)
public class Car {
	int number;					//필드=속성(멤버변수)
	int speed;					//필드=속성(멤버변수)
	String color;				//필드=속성(멤버변수)
	
	public void speedUp(){		//기능=함수=메소드
								//void 반환타입이 없음.
		speed +=10;
		//System.out.println("속도를 올린다!!");
	}
	
	public void speedDown(){	//기능=함수=메소드
		speed -=5;
		//System.out.println("속도를 내린다!!");
	}	
	
	//실제 메모리에 올려서 실행할수 있는 기본메소드 필요(main)
	//JVM에서 main 메소드 호출
	public static void main(String [] args){
		
		Car myCar = new Car(); 	//클래스를 객체화 할때는 -->클래스명 참조변수 = new 클래스명();
		Car yourCar = new Car();
		myCar.number = 1;
		myCar.speed = 10;
		myCar.color = "Black";
		
		yourCar.number = 2;
		yourCar.speed = 50;
		yourCar.color = "Red";
				
		System.out.println("myCar의 speed: " + myCar.speed);
		System.out.println("--------------------------");
		myCar.speedUp();
		myCar.speedDown();
		System.out.println(myCar.number);
		System.out.println(myCar.speed);		
		System.out.println(myCar.color);
				
		System.out.println("yourCar 의 speed: " + yourCar.speed);
		System.out.println("--------------------------");
		yourCar.speedUp();
		yourCar.speedDown();
		System.out.println(yourCar.number);
		System.out.println(yourCar.speed);		
		System.out.println(yourCar.color);
	}
}
