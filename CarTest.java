//한 파일에 안에는 public class는 하나만 존재해야 한다.
//public 타입을 가지는 클래스의 이름을 파일명으로 한다.
//main() 메소드를 가지는 클래스에 public 타입을 정해준다.
class CarDemo{
	//변수(variable)
	//1) 클래스 변수: static 붙은 변수(static 변수, 공유변수)
	//2) 인스턴스 변수:instance variable(static이 붙지 않은 변수 non-static)
	//   클래스변수와 인스턴스 변수를 멤버변수라고 한다.
	//   즉, 클래스 영역에 선언된 변수를 멤버 변수라고 한다.
	
	//3) 지역변수(local 변수) : 메서드 블럭안에 선언된 변수
	//                      메서드의 블럭이 끝나는 순간 생명을 다한다. 
	
	// 멤버변수는 초기화를 하지 않을 경우에 default 값이 들어간다.
	// ex> int default 값으로 0, String은 null값이 들어간다.
	// 지역변수는 꼭 초기화를 해야 한다.
	
	static String kind; 	//클래스 변수 선언
	int number;
	int speed;
	String color;
	
	// 생성자 : 인스턴스(객체)를 생성할 때 호출
	//	    - 생성자이름은 해당 클래스명과 동일
	//	    - 메소드와 비슷하나 반환타입이 없어야 한다.
	//	    - 멤버변수를 초기화할 때 사용한다.	
	public CarDemo(){
		kind = "소형차";
		int aa = 20; 	//지역변수 aa는 이 생성자 안에서만 유효하다.
	}

	public static void kind(){
		//System.out.println("소형차 입니다");
		System.out.println(kind);
	}
	public void speedUp(){
		kind = "대형차";
		speed +=10;
	}
	public void speedDown(){
		speed -=5;
	}
}

public class CarTest {
	static String kind1;
	
	public static void main(String[] args) {
		//클래스는 사용자 정의 타입이다.
		CarDemo myCar = new CarDemo();		//인스턴스 생성
		myCar.color = "흰색";
		System.out.println("클래스변수 kind: " + CarDemo.kind);
		
		CarDemo yourCar = new CarDemo();	//클래스변수 호출: 클래스명.변수명
		yourCar.color = "검은색";
		System.out.println("클래스변수 kind: " + CarDemo.kind);
		
		// 같은 클래스내에 있는 클래스변수를 호출하는 경우에는 클래스명을 생략할 수 있다.
		System.out.println(kind1);		
				
		// 인스턴스 변수의 호출 : 인스턴스를 생성한 후에 호출할 수  있다.
		// 인스턴스명(객체명).변수
		System.out.println("인스턴스변수 color: " + myCar.color);
		System.out.println("인스터스변수 color: " + yourCar.color);
		
		System.out.println("myCar의 차종은 : " + myCar.kind);
		System.out.println("yourCar의 차종은 : " + yourCar.kind);
		
		myCar.kind ="중형차";
		System.out.println("----------");
		System.out.println("myCar의 차종은 : " + myCar.kind);
		System.out.println("yourCar의 차종은 : " + yourCar.kind);		
		
		//지역변수
		int c = 100; 
		
		System.out.println("지역변수 : " + c);
		
		// System.out.println("지역변수 : " + myCar.aa); 
		
		//멤버 메소드의 호출 : 인스턴스를 생성한 후에 호출이 가능하다. (참조변수명.메소드명)
		System.out.println("myCar의 스피드 : " + myCar.speed);
		myCar.speedUp();
		myCar.speedUp();
		System.out.println("myCar의 스피드 : " + myCar.speed);
		myCar.speedDown();
		System.out.println("myCar의 스피드 : " + myCar.speed);
		
		System.out.println("----------------");
		System.out.println("yourCar의 스피드 : " + yourCar.speed);
		yourCar.speedUp();
		System.out.println("yourCar의 스피드 : " + yourCar.speed);
		
		//클래스 메소드 호출 : 클래스명.메소드명(인스턴스를 생성할 필요가 없다)
		//클래스메소드 안에는 클래스 변수만을 사용할 수 있다.
		CarDemo.kind();
	}
}